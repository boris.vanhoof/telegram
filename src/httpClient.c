#include <stdio.h>
#include <stdlib.h>
#include "httpClient.h"
#include <curl/curl.h>

static int refcount = 0;

struct httpClient {
    CURL *handle;
};

static void incref()
{
    refcount++;

    if (refcount == 1) {
        curl_global_init(0);
    }
}

static void decref()
{
    refcount--;

    if (!refcount) {
        curl_global_cleanup();
    }
}

httpResponse_t *httpClient_getRequest(httpClient_t client, const char *url)
{
    CURL *handle;
    CURLcode res;
    if (!client) {
        printf("missing parameters\n");
        goto error;
    }
    handle = ((struct httpClient *) client)->handle;

    curl_easy_setopt(handle, CURLOPT_URL, url);
    res = curl_easy_perform(handle);
    if (res != CURLE_OK) {
        printf("curl failed: %s\n", curl_easy_strerror(res));
        goto error;
    }
    printf("success!\n");

error:
    return NULL;
}

httpClient_t httpClient_create(void)
{
    struct httpClient *client = NULL;

    incref();

    client = calloc(1, sizeof(struct httpClient));
    if (!client) {
        printf("out of memory\n");
        goto error;
    }

    client->handle = curl_easy_init();
    if (!client->handle) {
        printf("failed to create curl handle\n");
        goto error;
    }

    return client;
error:
    free(client);
    return NULL;
}

void httpClient_destroy(httpClient_t client)
{
    if (!client) {
        goto out;
    }

    curl_easy_cleanup(((struct httpClient *) client)->handle);

    free(client);

    decref();
out:
    return;
}
