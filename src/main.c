#include <stdio.h>
#include "httpClient.h"

int main(int argc, char **argv)
{
    httpClient_t client = NULL;

    client = httpClient_create();

    (void) httpClient_getRequest(client, "http://www.google.com");

    httpClient_destroy(client);

    return 0;
}
