#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

typedef struct httpResponse {
    char *buffer;
    size_t buflen;
} httpResponse_t;

typedef struct httpClient *httpClient_t;

httpClient_t httpClient_create(void);
httpResponse_t *httpClient_getRequest(httpClient_t client, const char *url);
void httpClient_destroy(httpClient_t client);

#endif
